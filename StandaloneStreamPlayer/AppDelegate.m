//
//  AppDelegate.m
//  StandaloneStreamPlayer
//
//  Created by Ragu Vijaykumar on 4/7/13.
//  Copyright (c) 2013 Ragu Vijaykumar. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationWillFinishLaunching:(NSNotification *)aNotification {
    self.netflixVC = [[NetflixViewController alloc] initWithNibName:@"NetflixViewController" bundle:nil];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    [self.window setContentView:[self.netflixVC view]];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)theApplication {
    return YES;
}

- (IBAction)netflix:(id)sender {
    [self.netflixVC netflix:sender];
}

- (IBAction)hulu:(id)sender {
    [self.netflixVC hulu:sender];
}

- (IBAction)goBack:(id)sender {
    [self.netflixVC goBack:sender];
}

- (IBAction)goForward:(id)sender {
    [self.netflixVC goForward:sender];
}

- (IBAction)reload:(id)sender {
    [self.netflixVC reload:sender];
}

@end
