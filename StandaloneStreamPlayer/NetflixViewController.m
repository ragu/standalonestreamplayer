//
//  NetflixViewController.m
//  StandaloneStreamPlayer
//
//  Created by Ragu Vijaykumar on 4/7/13.
//  Copyright (c) 2013 Ragu Vijaykumar. All rights reserved.
//

#import "NetflixViewController.h"

@implementation NetflixViewController
static NSString* const kNetflixHomepage = @"https://www.netflix.com";
static NSString* const kHuluHomepage = @"https://www.hulu.com";

- (void)awakeFromNib {    
    [self.webView setCustomUserAgent:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A"];
    [self.webView setMainFrameURL:kNetflixHomepage];
}

- (IBAction)netflix:(id)sender {
    [self.webView setMainFrameURL:kNetflixHomepage];
}

- (IBAction)hulu:(id)sender {
    [self.webView setMainFrameURL:kHuluHomepage];
}

- (IBAction)goBack:(id)sender {
    [self.webView goBack:sender];
}

- (IBAction)goForward:(id)sender {
    [self.webView goForward:sender];
}

- (IBAction)reload:(id)sender {
    [self.webView reload:sender];
}

@end
