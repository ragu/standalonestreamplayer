//
//  AppDelegate.h
//  StandaloneStreamPlayer
//
//  Created by Ragu Vijaykumar on 4/7/13.
//  Copyright (c) 2013 Ragu Vijaykumar. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NetflixViewController.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (strong, nonatomic) NetflixViewController* netflixVC;

- (IBAction)netflix:(id)sender;
- (IBAction)hulu:(id)sender;

- (IBAction)goBack:(id)sender;
- (IBAction)goForward:(id)sender;
- (IBAction)reload:(id)sender;

@end
