//
//  NetflixViewController.h
//  StandaloneStreamPlayer
//
//  Created by Ragu Vijaykumar on 4/7/13.
//  Copyright (c) 2013 Ragu Vijaykumar. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@interface NetflixViewController : NSViewController

@property (weak, nonatomic) IBOutlet WebView* webView;

- (IBAction)netflix:(id)sender;
- (IBAction)hulu:(id)sender;

- (IBAction)goBack:(id)sender;
- (IBAction)goForward:(id)sender;
- (IBAction)reload:(id)sender;

@end
