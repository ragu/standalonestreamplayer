//
//  main.m
//  StandaloneStreamPlayer
//
//  Created by Ragu Vijaykumar on 4/7/13.
//  Copyright (c) 2013 Ragu Vijaykumar. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
